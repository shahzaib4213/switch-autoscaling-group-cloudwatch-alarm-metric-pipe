#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

enable_debug() {
    if [[ "${DEBUG}" == "true" ]]; then
        info "Enabling debug mode."
        set -x
    fi
}
enable_debug

# required parameters
CLOUDWATCH_ALARM_NAME=${CLOUDWATCH_ALARM_NAME:?'CLOUDWATCH_ALARM_NAME environment variable missing.'}
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?"AWS_ACCESS_KEY_ID environment variable missing."}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?"AWS_SECRET_ACCESS_KEY environment variable missing."}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?"AWS_DEFAULT_REGION environment variable missing."}
APPLICATION_NAME=${APPLICATION_NAME:?"APPLICATION_NAME environment variable missing."}
DEPLOYMENT_GROUP=${DEPLOYMENT_GROUP:?"DEPLOYMENT_GROUP environment variable missing."}

# default parameters
DEBUG=${DEBUG:="false"}

AUTO_SCALING_GROUP_NAME=$(aws deploy get-deployment-group --application-name $APPLICATION_NAME --deployment-group-name $DEPLOYMENT_GROUP --query 'deploymentGroupInfo.autoScalingGroups[*].name' --output text)

ALARM_JSON=$(aws cloudwatch describe-alarms --alarm-names $CLOUDWATCH_ALARM_NAME --output json)

METRIC_NAME=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].MetricName')
NAMESPACE=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].Namespace')
STATISTIC=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].Statistic')
PERIOD=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].Period')
THRESHOLD=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].Threshold')
COMPARISON_OPERATOR=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].ComparisonOperator')
EVALUATION=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].EvaluationPeriods')
ALARM_ACTIONS=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].AlarmActions[0]')

DIMENSIONS_JSON=$(echo $ALARM_JSON | jq -r '.MetricAlarms[0].Dimensions')

INTRESTING_DIMENSION_INDEX=$(echo $DIMENSIONS_JSON | jq 'map(.Name == "AutoScalingGroupName") | index(true)')

if [[ "$INTRESTING_DIMENSION_INDEX" == "null" ]]; then
    info "No auto scaling group to update"
else
    PREV_AUTOSCALING_GROUP=$(echo $DIMENSIONS_JSON | jq -r ".[$INTRESTING_DIMENSION_INDEX].Value")
    if [[ "$PREV_AUTOSCALING_GROUP" == "$AUTO_SCALING_GROUP_NAME" ]]; then
        info "Already updated"
    else
        DIMENSIONS=$(echo $DIMENSIONS_JSON | jq -r -c ".[$INTRESTING_DIMENSION_INDEX].Value=\"$AUTO_SCALING_GROUP_NAME\"")
        aws cloudwatch put-metric-alarm --alarm-name $CLOUDWATCH_ALARM_NAME --metric-name $METRIC_NAME --namespace $NAMESPACE --statistic $STATISTIC --period $PERIOD --threshold $THRESHOLD --comparison-operator $COMPARISON_OPERATOR --dimensions $DIMENSIONS --evaluation-periods $EVALUATION --alarm-actions $ALARM_ACTIONS --region $AWS_DEFAULT_REGION
    fi

fi

if [[ "$?" == "0" ]]; then
    success "Success!"
else
    fail "Error!"
fi
